#pragma once
#include <stdlib.h>
#include <iostream>
#include <time.h>
#include "MEMALLOC_LEAKTEST.h"

template<typename T>
class DynamicArray
{
public:
	DynamicArray(int a_space);
	DynamicArray(const DynamicArray &arr);
	~DynamicArray();

	class Iterator
	{
		Iterator() {};
		Iterator(int a_position) {};

		const Iterator& operator+(const T a_value) { dynArr + a_value; }
		const Iterator& operator-(const T a_value) { dynArr - a_value; }
		const Iterator& operator++() { dynArr + 1; }
		const Iterator& operator--() { dynArr - 1; }

		T& operator*() { return *dynArr; }
		bool operator == (const Iterator& rhs) { return dynArr == rhs.dynArr; }
		bool operator != (const Iterator& rhs) { return dynArr != rhs.dynArr; }

		int* dynArr;
	};

	void PushFront(T a_value);
	void PushPosition(int a_position, T a_value);
	void PushBack(T a_value);
	void PopFront();
	void PopPosition(int a_position);
	void PopPositionOrdered(int a_position);
	void PopBack();

	void ReallocateSpace(int a_space);
	void Clear();

	void Sort();
	bool FindElement(T a_value);
	int  FindElementPosition(T a_value);
	void ConcatenateArray(T* a_arr, int a_size);
	void ShuffleArray();
	void Rotate(T a_value);

	int GetSize() { return m_usedElements; };
	void PrintArray();

	T& operator[] (int a_index);

	T GetElement(int a_position);

private:
	T* m_arr;
	size_t m_allocatedElements = 0;
	size_t m_usedElements = 0;

	void QuickSort(T arr[], int low, int high);
	int Partition(T arr[], int low, int high);
	void Swap(T& a, T& b);
};

template<typename T>
DynamicArray<T>::DynamicArray(int a_space)
{
	m_arr = DBG_NEW T[a_space];
	m_allocatedElements = a_space;
	m_usedElements = 0;
}

template<typename T>
DynamicArray<T>::DynamicArray(const DynamicArray &arr)
{
	m_arr = arr.m_arr;
	m_allocatedElements = arr.m_allocatedElements;
	m_usedElements = arr.m_usedElements;
}

template<typename T>
DynamicArray<T>::~DynamicArray()
{
	delete[] m_arr;
}

template<typename T>
void DynamicArray<T>::PushFront(T a_value)
{
	// Increase memory if the array is full
	if (m_usedElements >= m_allocatedElements)
	{
		ReallocateSpace(m_allocatedElements * 2);
	}
	// Go to end of array, shuffle all elements forward 1
	// Going backwards through the list
	T* arrEnum = m_arr;
	arrEnum += m_usedElements;
	for (int i = 0; i < m_usedElements; i++)
	{
		*arrEnum = *(arrEnum - 1);
		arrEnum--;
	}
	// arrEnum is now the start of the array
	*arrEnum = a_value;
	m_usedElements++;
}

template<typename T>
void DynamicArray<T>::PushPosition(int a_position, T a_value)
{
	// Increase memory if the array is full
	if (m_usedElements >= m_allocatedElements)
	{
		ReallocateSpace(m_allocatedElements * 2);
	}
	// Go to end of array, shuffle all elements forward 1
	// Going backwards through the list UNTIL the wanted position is reached
	int* arrEnum = m_arr;
	arrEnum += m_usedElements;
	for (int i = 0; i < (m_usedElements - a_position + 1); i++)
	{
		*arrEnum = *(arrEnum - 1);
		arrEnum--;
	}
	// arrEnum is now at the correct insertion position
	*arrEnum = a_value;
	m_usedElements++;
}

template<typename T>
void DynamicArray<T>::PushBack(T a_value)
{
	// Increase memory if the array is full
	if (m_usedElements >= m_allocatedElements)
	{
		ReallocateSpace(m_allocatedElements * 2);
	}
	// Go to end of array, and place item after last element.
	T* arrEnum = m_arr;
	arrEnum += m_usedElements;
	*arrEnum = a_value;
	m_usedElements++;
}

template<typename T>
void DynamicArray<T>::PopFront()
{
	// Shuffle all elements of the array down
	// Element at 0 will be replaced while moving down
	int* arrEnum = m_arr;
	arrEnum++;
	for (int i = 0; i < (m_usedElements - 1); i++)
	{
		*(arrEnum - 1) = *arrEnum;
		arrEnum++;
	}
	m_usedElements--;
}

template<typename T>
void DynamicArray<T>::PopPosition(int a_position)
{
	// Replace element at position with element at end of array,
	// then shorten used elements of array by 1
	int* arrEnum = m_arr;
	arrEnum += a_position;
	*arrEnum = *(m_arr += m_usedElements - 1);
	m_usedElements--;
}

template<typename T>
void DynamicArray<T>::PopPositionOrdered(int a_position)
{
	// Shuffle all elements of the array down
	// Element at POSITION will be replaced while moving down
	int* arrEnum = m_arr;
	arrEnum += a_position;
	for (int i = 0; i < (m_usedElements - a_position); i++)
	{
		*(arrEnum - 1) = *arrEnum;
		arrEnum++;
	}
	m_usedElements--;
}

template<typename T>
void DynamicArray<T>::PopBack()
{
	// Reduce used elements by 1
	m_usedElements--;
}

template<typename T>
void DynamicArray<T>::ReallocateSpace(int a_space)
{
	// Make reference to DBG_NEW array
	T* newArr = DBG_NEW T[a_space];
	// oldArr += (m_allocatedElements - 1);
	// Run through and make DBG_NEW elements equal to old elements
	// AND break if the DBG_NEW capacity is exceeded (cut off old data)
	for (int i = 0; i < m_usedElements; i++)
	{
		if (i > a_space)
			break;
		newArr[i] = m_arr[i];
	}
	// Delete memory
	delete[] m_arr;
	m_arr = nullptr;
	// Make allocated elements the total space; and make the actual array = DBG_NEW array.
	m_allocatedElements = a_space;
	m_arr = newArr;
}

template<typename T>
void DynamicArray<T>::Clear()
{
	// # of used elements set to 0
	m_usedElements = 0;
}

template<typename T>
void DynamicArray<T>::Sort()
{
	QuickSort(m_arr, 0, m_usedElements - 1);
}

template<typename T>
bool DynamicArray<T>::FindElement(T a_value)
{
	for (int i = 0; i < m_usedElements - 1; i++)
	{
		if (m_arr[i] == a_value)
			return true;
	}
	return false;
}

template<typename T>
inline int DynamicArray<T>::FindElementPosition(T a_value)
{
	for (int i = 0; i < m_usedElements - 1; i++)
	{
		if (m_arr[i] == a_value)
			return i;
	}
}

template<typename T>
T DynamicArray<T>::GetElement(int a_position)
{
	return m_arr[a_position];
}

template<typename T>
inline void DynamicArray<T>::ConcatenateArray(T * a_arr, int a_size)
{
	// Push all elements of given array into this array to back.
	for (int i = 0; i < (a_size - 1); i++)
	{
		PushBack(a_arr);
		a_arr++;
	}
}

template<typename T>
inline void DynamicArray<T>::ShuffleArray()
{
	for (int i = (m_usedElements - 1); i > 0; i--)
	{
		int j = (rand() % m_usedElements);
		while (!(j <= i))
		{
			j = (rand() % m_usedElements);
		}
		Swap(m_arr[j], m_arr[i]);
	}
}

template<typename T>
void DynamicArray<T>::Rotate(T a_value)
{
	if (a_value > 0)
	{
		for (int i = 0; i < a_value; i++)
		{
			T tempLast = m_arr[m_usedElements - 1];
			T* arrEnum = &m_arr[m_usedElements - 1];
			for (int i = 0; i < (m_usedElements - 1); i++)
			{
				*arrEnum = *(arrEnum - 1);
				arrEnum--;
			}
			*m_arr = tempLast;
		}
	}
	else if (a_value < 0)
	{
		a_value = abs(a_value);
		for (int i = 0; i < a_value; i++)
		{
			T tempFirst = *m_arr;
			T* arrEnum = m_arr;
			for (int i = 0; i < (m_usedElements - 1); i++)
			{
				*arrEnum = *(arrEnum + 1);
				arrEnum++;
			}
			*arrEnum = tempFirst;
		}
	}
}

template<typename T>
inline void DynamicArray<T>::PrintArray()
{
	for (int i = 0; i < m_usedElements; i++)
	{
		std::cout << i + 1 << ". " << m_arr[i] << std::endl;
	}
	std::cout << std::endl;
}

template<typename T>
inline T & DynamicArray<T>::operator[](int a_index)
{
	return m_arr[a_index];
}

template<typename T>
void DynamicArray<T>::QuickSort(T arr[], int low, int high)
{
	if (low < high)
	{
		int partIndex = Partition(arr, low, high);

		QuickSort(arr, low, partIndex - 1);
		QuickSort(arr, partIndex + 1, high);
	}
}

template<typename T>
int DynamicArray<T>::Partition(T arr[], int low, int high)
{
	int pivot = arr[high];
	int i = (low - 1);

	for (int j = low; j <= high - 1; j++)
	{
		if (arr[j] <= pivot)
		{
			i++;
			Swap(arr[i], arr[j]);
		}
	}
	Swap(arr[i + 1], arr[high]);
	return i + 1;
}

template<typename T>
void DynamicArray<T>::Swap(T& a, T& b)
{
	T temp = a;
	a = b;
	b = temp;
}