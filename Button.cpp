#include "Button.h"
#include "MEMALLOC_LEAKTEST.h"

Button::Button(int id, const char* buttonText, float x, float y, float width, float height, float r, float g, float b, float a)
{
	m_id = id;

	// Button label
	strcpy_s(m_buttonText, 64, buttonText);

	// Font for label
	m_font = DBG_NEW aie::Font("./font/consolas.ttf", 24);

	// Position & size of button
	m_posX = x;
	m_posY = y;
	m_width = width;
	m_height = height;

	// Colour of button
	colour.R = r;
	colour.G = g;
	colour.B = b;
	colour.A = a;

	// Allows resetting to default, like un-hovering
	colourDefault = colour;
}

Button::~Button()
{
	delete m_font;
}

void Button::Draw(aie::Renderer2D* renderer)
{
	renderer->setRenderColour(colour.R, colour.G, colour.B, colour.A);
	renderer->drawBox(m_posX, m_posY, m_width, m_height);

	//Calculating the centred text position is a bit fiddly.
	float textWidth = m_font->getStringWidth(m_buttonText);
	float textHeight = m_font->getStringHeight(m_buttonText);
	float centredPosX = m_posX - (textWidth * 0.5f) + 2;
	float centredPosY = m_posY - (textHeight * 0.5f) + 5;
	//Draw text on the button.
	renderer->setRenderColour(0, 0, 0);
	renderer->drawText(m_font, m_buttonText, centredPosX, centredPosY);

	renderer->setRenderColour(1, 1, 1);
}

bool Button::Update()
{
	aie::Input* input = aie::Input::getInstance();

	// Get mouse pos
	int mouseX = input->getMouseX();
	int mouseY = input->getMouseY();

	float lft = m_posX - (m_width * 0.5f);
	float rgt = m_posX + (m_width * 0.5f);
	float bot = m_posY - (m_height * 0.5f);
	float top = m_posY + (m_height * 0.5f);

	if (mouseX > lft && mouseX < rgt &&
		mouseY > bot && mouseY < top)
		return /*input->wasMouseButtonPressed(0)*/ true;
	return false;
}

bool Button::GetHovering()
{
	aie::Input* input = aie::Input::getInstance();

	// Get mouse pos
	int mouseX = input->getMouseX();
	int mouseY = input->getMouseY();

	float lft = m_posX - (m_width * 0.5f);
	float rgt = m_posX + (m_width * 0.5f);
	float bot = m_posY - (m_height * 0.5f);
	float top = m_posY + (m_height * 0.5f);

	if (mouseX > lft && mouseX < rgt &&
		mouseY > bot && mouseY < top)
		return input->wasMouseButtonPressed(0);
	return false;
}

Colour Button::GetColour()
{
	return colour;
}

void Button::SetColour(float r, float g, float b, float a)
{
	colour.R = r;
	colour.G = g;
	colour.B = b;
	colour.A = a;
}
