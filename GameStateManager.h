#pragma once
#include "DynamicArray.h"
#include "GameState.h"

class GameStateManager
{
public:
	GameStateManager(unsigned int stateCount) 
	{
		m_pushedStates = DBG_NEW DynamicArray<GameState*>(2);
		m_stateStack = DBG_NEW DynamicArray<GameState*>(5);
		m_registeredStates = DBG_NEW DynamicArray<GameState*>(5);
	}

	~GameStateManager() 
	{ 
		for (int i = 0; i < m_registeredStates->GetSize(); i++) delete m_registeredStates->GetElement(i);

		delete m_pushedStates;
		delete m_stateStack;
		delete m_registeredStates;
	}

	void RegisterState(int id, GameState* state);
	void PushState(int id);
	void PopState();

	void Update(float deltaTime, aie::Input* input);

	void Draw(aie::Renderer2D* renderer, HashTable<aie::Texture*>* textureLs);

	int ActiveStateCount() { return m_stateStack->GetSize(); }
	GameState* GetTopState() { return m_stateStack->GetElement(m_stateStack->GetSize()); }
	GameState* GetState(int id) { return m_registeredStates->GetElement(id); }

private:
	DynamicArray<GameState*>* m_pushedStates;
	bool m_popState = false;

	DynamicArray<GameState*>* m_stateStack;
	DynamicArray<GameState*>* m_registeredStates;
};

