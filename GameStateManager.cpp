#include "GameStateManager.h"
#include "MEMALLOC_LEAKTEST.h"

void GameStateManager::RegisterState(int id, GameState* state)
{
	m_registeredStates->PushBack(state);
}
void GameStateManager::PushState(int id)
{
	m_pushedStates->PushBack(m_registeredStates->GetElement(id));
}
void GameStateManager::PopState()
{
	m_popState = true;
}

void GameStateManager::Update(float deltaTime, aie::Input* input)
{
	while (m_popState)
	{
		m_popState = false;

		// Deactivate current top
		m_stateStack->GetElement(m_stateStack->GetSize()-1)->Exit();
		auto temp = m_stateStack->GetElement(m_stateStack->GetSize()-1);
		m_stateStack->PopBack();
		temp->OnPopped();

		// Activate previous state, if it exists
		if (m_stateStack->GetSize() != 0)
			m_stateStack->GetElement(m_stateStack->GetSize()-1)->Enter();
	}

	for (int i = 0; i < m_pushedStates->GetSize(); i++)
	{
		// Deactivate previous top
		if (m_stateStack->GetSize() != 0)
			m_stateStack->GetElement(m_stateStack->GetSize()-1)->Exit();

		// Activate DBG_NEW top
		m_pushedStates->GetElement(i)->OnPushed();
		m_stateStack->PushBack(m_pushedStates->GetElement(i));
		m_stateStack->GetElement(m_stateStack->GetSize()-1)->Enter();
	}
	m_pushedStates->Clear();

	//for (int i = 0; i < m_stateStack->GetSize(); i++)
	//	m_stateStack->GetElement(i)->OnUpdate(deltaTime, input);
	
	GameState* topStack = m_stateStack->GetElement(m_stateStack->GetSize() - 1);

	// Check if topmost stack exists
	if (topStack && m_stateStack->GetSize() > 0)
	{
		// Update topmost stack
		topStack->OnUpdate(deltaTime, input);

		// If state needs to be popped, it will be done next frame
		if (m_stateStack->GetSize() > 0 && !topStack->IsActive())
			m_popState = true;

		// If state needs to be pushed, it will be done next frame
		// Only pushes if push request is from topmost state
		if (topStack->m_pushStateGo)
		{
			topStack->PushStateReset();
			PushState(topStack->m_pushState);
		}
	}
}

void GameStateManager::Draw(aie::Renderer2D* renderer, HashTable<aie::Texture*>* textureTable)
{
	for (int i = 0; i < m_stateStack->GetSize(); i++)
		m_stateStack->GetElement(i)->OnDraw(renderer, textureTable);
	// Reset render colour
	renderer->setRenderColour(1, 1, 1);
}