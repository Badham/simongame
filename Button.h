#pragma once
#include "Renderer2D.h"
#include "Font.h"
#include "Input.h"
#include <string.h>
#include "GameDefinitions.h"

class Button
{
public:
	Button(int id, const char* buttonText, float x, float y, float width, float height, float r = 0.0f, float g = 0.0f, float b = 0.0f, float a = 1.0f);

	~Button();

	void Draw(aie::Renderer2D* renderer);
	bool Update();

	int GetID() { return m_id; }

	bool GetHovering();

	Colour GetColour();
	void SetColour(float r, float g, float b, float a = 1.0f);
	void ResetColour() { colour = colourDefault; }

private:
	int m_id = 0;

	aie::Font* m_font;
	char m_buttonText[64];

	float m_posX;
	float m_posY;
	float m_width;
	float m_height;

	Colour colour;
	Colour colourDefault;
};

