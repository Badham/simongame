#pragma once
#include "Renderer2D.h"
#include "Input.h"
#include "GameDefinitions.h"
#include "HashTable.h"

class GameState
{
	friend class GameStateManager;
public:
	GameState() {};
	virtual ~GameState() {};
	bool IsActive() const { return m_active; }

protected:
	virtual void OnUpdate(float deltaTime, aie::Input* input) = 0;
	virtual void OnDraw(aie::Renderer2D* renderer, HashTable<aie::Texture*>* textureLs) = 0;

	void Enter() { m_active = true; OnEnter(); }
	void Exit() { m_active = false; OnExit(); }

	virtual void OnEnter() {}
	virtual void OnExit() {}
	virtual void OnPushed() {}
	virtual void OnPopped() {}

	// Used for pushing state for the state manager
	void PushStateInternal(eGameState a_state);
	void PushStateReset();

	bool		m_pushStateGo = false;
	eGameState	m_pushState = eGameState::STATE_COUNT;

private:
	bool		m_active = false;
};

