#include "SimonGameApp.h"
#include "GameDefinitions.h"
#include "MEMALLOC_LEAKTEST.h"

int main() 
{
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_FILE | _CRTDBG_MODE_WNDW);
	_CrtSetReportFile(_CRT_ASSERT, _CRTDBG_FILE_STDERR);

	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	// allocation
	auto app = DBG_NEW SimonGameApp();

	// initialise and loop
	app->run("AIE", WINDOW_WIDTH, WINDOW_HEIGHT, false);

	// deallocation
	delete app;

	return 0;
}