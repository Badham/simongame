#pragma once
#include "GameState.h"
#include "UIManager.h"


class MenuState : public GameState
{
public:
	MenuState();
	virtual ~MenuState();

	virtual void OnUpdate(float deltaTime, aie::Input* input);
	virtual void OnDraw(aie::Renderer2D* renderer, HashTable<aie::Texture*>* textureLs);

private:
	UIManager*	m_uiManager;

	bool		m_mouseDown = false;
};

