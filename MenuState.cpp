#include "MenuState.h"
#include "MEMALLOC_LEAKTEST.h"

MenuState::MenuState() 
{
	m_uiManager = DBG_NEW UIManager();

	// Title text
	m_uiManager->AddTextBox("SIMON SAYS", WINDOW_HCENTER, (WINDOW_WCENTER + 300), 300, 100, 0.8f, 0.6f, 0.7f);

	// Buttons
	m_uiManager->AddButton(0, "EASY", WINDOW_HCENTER, (WINDOW_WCENTER + 200), 200, 100, 0.5f, 1.0f, 0.5f);
	m_uiManager->AddButton(1, "HARD", WINDOW_HCENTER, (WINDOW_WCENTER + 100), 200, 100, 1.0f, 1.0f, 0.5f);
	m_uiManager->AddButton(2, "NIGHTMARE", WINDOW_HCENTER, WINDOW_WCENTER, 200, 100, 1.0f, 0.5f, 0.5f);
	m_uiManager->AddButton(3, "EXIT", WINDOW_HCENTER, (WINDOW_WCENTER -200), 150, 75, 1.0f, 0.0f, 0.0f);
}

MenuState::~MenuState() 
{
	delete m_uiManager;
}

void MenuState::OnUpdate(float deltaTime, aie::Input* input)
{
	// Gets the button the player is currently hovering over
	Button* buttonHover = m_uiManager->GetButtonList();
	Button** buttonHoverPtr = &buttonHover;
	m_uiManager->Update(buttonHoverPtr);

	// Only apply the hover effect if the user is hovering a button & the button is interactable
	if (buttonHover)
		buttonHover->SetColour((buttonHover->GetColour().R + 0.2f), (buttonHover->GetColour().G + 0.2f), (buttonHover->GetColour().B + 0.2f));

	if (input->isMouseButtonDown(0) && !m_mouseDown)
	{
		m_mouseDown = true;
		// Make sure the user is actually clicking a button before checking if it's correct
		if (buttonHover)
		{
			switch (buttonHover->GetID())
			{
			case 0:
				PushStateInternal(eGameState::INGAME_EASY);
				break;
			case 1:
				PushStateInternal(eGameState::INGAME_HARD);
				break;
			case 2:
				PushStateInternal(eGameState::INGAME_NIGHTMARE);
				break;
			case 3:
				Exit();
				break;
			default:
				break;
			}
		}
	}
	// Check if user has lifted the mouse button
	if ((!input->isMouseButtonDown(0)) && m_mouseDown)
		m_mouseDown = false;
}

void MenuState::OnDraw(aie::Renderer2D* renderer, HashTable<aie::Texture*>* textureTable)
{
	renderer->drawSprite(textureTable->GetData("Menu BG"), WINDOW_HCENTER, WINDOW_WCENTER, WINDOW_WIDTH, WINDOW_HEIGHT);
	//renderer->drawSprite(textureLs[eTextureArray::SIMON_BACK], WINDOW_HCENTER, WINDOW_WCENTER, 512, 512);
	m_uiManager->Draw(renderer);
	// Refresh colour so buttons don't get perpetually darker
	m_uiManager->ButtonRefresh();
}