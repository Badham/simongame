#include "SimonManager.h"
#include "MEMALLOC_LEAKTEST.h"

SimonManager::SimonManager(eDifficultyLevel difficulty)
{
	m_difficulty = difficulty;
	m_sequenceList = DBG_NEW DynamicArray<int>(16);

	SequenceInit(difficulty);
}

SimonManager::~SimonManager()
{
	delete m_sequenceList;
}

void SimonManager::DifficultyIncrease(eDifficultyLevel difficulty)
{
	switch (difficulty)
	{
	case EASY:
		AddRandFlash(1);
		break;
	case HARD:
		AddRandFlash(2);
		break;
	case NIGHTMARE:
		AddRandFlash(5);
		break;
	default:
		break;
	}
	m_sequencePos = 0;
}

bool SimonManager::checkButtonPress(Button* button)
{
	if ((*m_sequenceList)[m_sequencePos] == button->GetID())
		return true;
	return false;
}

void SimonManager::RefreshSequence()
{
	m_sequenceList->Clear();
	SequenceInit(m_difficulty);
	m_sequencePos = 0;
}

void SimonManager::SequenceInit(eDifficultyLevel difficulty)
{
	switch (difficulty)
	{
	case EASY:
		AddRandFlash(3);
		break;
	case HARD:
		AddRandFlash(5);
		break;
	case NIGHTMARE:
		AddRandFlash(20);
		break;
	default:
		break;
	}
}

void SimonManager::AddRandFlash(int count)
{
	for (int i = 0; i < count; i++)
		m_sequenceList->PushBack(rand() % 4);
}

