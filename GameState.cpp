#include "GameState.h"

void GameState::PushStateInternal(eGameState a_state)
{
	m_pushState = a_state;
	m_pushStateGo = true;
}

void GameState::PushStateReset()
{
	m_pushStateGo = false;
}
