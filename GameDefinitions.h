#pragma once

#define WINDOW_HEIGHT 720
#define WINDOW_WIDTH 1280

#define WINDOW_HCENTER (WINDOW_WIDTH / 2)
#define WINDOW_WCENTER (WINDOW_HEIGHT / 2)

enum eGameState
{
	MENU,
	INGAME_EASY,
	INGAME_HARD,
	INGAME_NIGHTMARE,
	STATE_COUNT
};

enum eTextureArray
{
	BG,
	SIMON_BACK,
	SIMON_QUARTER,
	TEXTURE_COUNT
};

enum eDifficultyLevel
{
	EASY,
	HARD,
	NIGHTMARE
};

struct Vector2D
{
	float x;
	float y;
};

struct Colour
{
	float R = 0.0f;
	float G = 0.0f;
	float B = 0.0f;

	float A = 1.0f;
};