#include "UIManager.h"
#include "MEMALLOC_LEAKTEST.h"

UIManager::UIManager()
{
	m_buttonArray = DBG_NEW DynamicArray<Button*>(4);
	m_textBoxArray = DBG_NEW DynamicArray<TextBox*>(4);
}

UIManager::~UIManager()
{
	for (int i = 0; i < m_buttonArray->GetSize(); i++) { delete m_buttonArray->GetElement(i); }
	for (int i = 0; i < m_textBoxArray->GetSize(); i++) { delete m_textBoxArray->GetElement(i); }
	delete m_buttonArray;
	delete m_textBoxArray;
}

void UIManager::Draw(aie::Renderer2D* renderer)
{
	// Draw every button in the array
	for (int i = 0; i < m_buttonArray->GetSize(); i++)
		if((*m_buttonArray)[i])
			(*m_buttonArray)[i]->Draw(renderer);
	// Draw text boxes
	for (int i = 0; i < m_textBoxArray->GetSize(); i++)
		if ((*m_textBoxArray)[i])
			(*m_textBoxArray)[i]->Draw(renderer);
}

void UIManager::ButtonRefresh()
{
	for (int i = 0; i < m_buttonArray->GetSize(); i++)
		(*m_buttonArray)[i]->ResetColour();
}

// Update sets the given ptr-ptr to the button being hovered over
void UIManager::Update(Button** buttonHover)
{
	for (int i = 0; i < m_buttonArray->GetSize(); i++)
	{
		if ((*m_buttonArray)[i]->Update())
		{
			*buttonHover = (*m_buttonArray)[i];
			break;
		}
		*buttonHover = nullptr;
	}
}

void UIManager::DisplaySequence(DynamicArray<int>* sequence, eDifficultyLevel difficulty, float deltaTime)
{
	// Add time to display time counter
	m_displayTime += deltaTime;
	// Time between both showing the next sequence, next flash in a sequence or before showing the initial sequence.
	if (m_displayTime >= 1.0f - (difficulty * 0.3f))
	{
		m_displayTime = 0.0f;
		m_sequencePos++;
	}
	// Initial pause before displaying the sequence after either starting a difficulty or finishing a sequence
	if (m_sequencePos == -1)
		return;
	// Makes buttons highlighted based on sequence
	Button* buttonHighlight = nullptr;
	for (int i = 0; i < m_buttonArray->GetSize(); i++)
	{
		if ((*m_buttonArray)[i]->GetID() == (*sequence)[m_sequencePos])
		{
			buttonHighlight = (*m_buttonArray)[i];
			break;
		}
	}
	// No more buttons in the sequence, end here and prepare for user input
	if (m_sequencePos >= sequence->GetSize())
	{
		m_sequenceComplete = true;
		m_sequencePos = 0;
	}
	// If there's a button to highlight
	// also stops highlighting in the last 1/5th of the flash for spacing
	else if (buttonHighlight)
	{
		// De-highlight button in the last 1/5th of the display time
		if (m_displayTime <= 0.8f - (difficulty * 0.25f))
		{
			buttonHighlight->SetColour((buttonHighlight->GetColour().R + 0.2f), (buttonHighlight->GetColour().G + 0.2f), (buttonHighlight->GetColour().B + 0.2f));
		}
	}
}

bool UIManager::GetSequenceComplete()
{
	return m_sequenceComplete;
}

void UIManager::SetSequenceComplete(bool state)
{
	m_sequenceComplete = state;
}

void UIManager::AddButton(int id, const char * buttonText, float x, float y, float width, float height, float r, float g, float b, float a)
{
	m_buttonArray->PushBack(DBG_NEW Button(id, buttonText, x, y, width, height, r, g, b, a));
}

void UIManager::AddTextBox(const char* buttonText, float x, float y, float width, float height, float r, float g, float b, float a)
{
	m_textBoxArray->PushBack(DBG_NEW TextBox(buttonText, x, y, width, height, r, g, b, a));
}

void UIManager::RemoveButton(int id)
{
	
}

void UIManager::RemoveTextbox()
{
	if (m_textBoxArray->GetSize() > 1)
		m_textBoxArray->PopBack();
}

void UIManager::RefreshUIManager()
{
	m_displayTime = 0.0f;
	m_sequencePos = -1;
	m_sequenceComplete = false;

	m_sequenceFailed = false;
}
