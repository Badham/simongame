#pragma once

#include "Application.h"
#include "Renderer2D.h"
#include "GameStateManager.h"
#include "HashTable.h"

class SimonGameApp : public aie::Application 
{
public:

	SimonGameApp();
	virtual ~SimonGameApp();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

protected:
	// Bootstrap
	aie::Renderer2D*			m_2dRenderer;
	aie::Font*					m_font;
	HashTable<aie::Texture*>*	m_textureTable;

	// Simon Game
	GameStateManager*			m_gameStateManager;
};