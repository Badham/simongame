#include "PlayingState.h"
#include "MEMALLOC_LEAKTEST.h"

PlayingState::PlayingState(eDifficultyLevel difficulty, HashTable<aie::Texture*>* textureTable)
{
	m_simonManager = DBG_NEW SimonManager(difficulty);
	m_uiManager = DBG_NEW UIManager();
	m_difficulty = difficulty;

	switch (difficulty)
	{
	case EASY:
		m_background = textureTable->GetData("Easy BG");
			break;
	case HARD:
		m_background = textureTable->GetData("Hard BG");
		break;
	case NIGHTMARE:
		m_background = textureTable->GetData("Nightmare BG");
		break;
	default:
		m_background = textureTable->GetData("Easy BG");
		break;
	}

	m_score = 0;

	m_uiManager->AddButton(0, "RED", (WINDOW_HCENTER - 100), (WINDOW_WCENTER - 100), 200, 200, 0.8f, 0.0f, 0.0f);
	m_uiManager->AddButton(1, "GREEN", (WINDOW_HCENTER + 100), (WINDOW_WCENTER - 100), 200, 200, 0.0f, 0.8f, 0.0f);
	m_uiManager->AddButton(2, "BLUE", (WINDOW_HCENTER - 100), (WINDOW_WCENTER + 100), 200, 200, 0.0f, 0.0f, 0.8f);
	m_uiManager->AddButton(3, "YELLOW", (WINDOW_HCENTER + 100), (WINDOW_WCENTER + 100), 200, 200, 0.8f, 0.8f, 0.0f);
	// Exit button
	m_uiManager->AddButton(4, "EXIT", 100, 50, 100, 50, 0.8f, 0.3f, 0.3f);
	// Text boxes
	m_uiManager->AddTextBox("SCORE: 0", WINDOW_HCENTER, (WINDOW_WCENTER + 300), 400, 100, 0.2f, 0.2f, 0.2f);
}

void PlayingState::OnUpdate(float deltaTime, aie::Input* input)
{
	// The user failed the sequence, halt input and say game over
	if (m_failedSequence && !m_gameoverVisible)
	{
		m_uiManager->AddTextBox("GAME OVER", WINDOW_HCENTER, (WINDOW_WCENTER - 300), 200, 50, 0.5f, 0.1f, 0.1f);
		m_gameoverVisible = true;
	}

	if (m_sequencePreview)
	{
		m_uiManager->DisplaySequence(m_simonManager->GetSequence(), m_simonManager->GetDifficulty(), deltaTime);
		// If preview sequence is complete, go to user input
		if (m_uiManager->GetSequenceComplete())
		{
			m_uiManager->SetSequenceComplete(false);
			m_sequencePreview = false;
		}
	}
	else
	{
		// Gets the button the player is currently hovering over
		Button* buttonHover = m_uiManager->GetButtonList();
		Button** buttonHoverPtr = &buttonHover;
		m_uiManager->Update(buttonHoverPtr);

		// Only apply the hover effect if the user is hovering a button & the button is interactable
		if (buttonHover)
			buttonHover->SetColour((buttonHover->GetColour().R + 0.2f), (buttonHover->GetColour().G + 0.2f), (buttonHover->GetColour().B + 0.2f));

		if (input->isMouseButtonDown(0) && !m_mouseDown)
		{
			m_mouseDown = true;
			// Make sure the user is actually clicking a button before checking if it's correct
			if (buttonHover)
			{
				if (!m_failedSequence && (buttonHover->GetID() == m_simonManager->GetSequencePosID()))
				{
					m_simonManager->ContinueSequence();
					m_score += (10 + (10 * m_difficulty));
					// Only 1 text element on playing state, will be first element
					TextBox* scoreTextBox = m_uiManager->GetTextBoxList();
					char scoreText[64];
					snprintf(scoreText, 64, "SCORE: %i", m_score);
					scoreTextBox->SetText(scoreText);
				}
				// User pressed the exit button, leave
				else if (buttonHover->GetID() == 4)
					Exit();
				// Pressed the wrong button, enter failed sequence
				else
					m_failedSequence = true;
			}
		}
		// Check if user has lifted the mouse button
		if ((!input->isMouseButtonDown(0)) && m_mouseDown)
			m_mouseDown = false;
	}
	if (m_simonManager->EndOfSequence())
	{
		m_simonManager->DifficultyIncrease(m_difficulty);
		m_sequencePreview = true;
	}
}

void PlayingState::OnDraw(aie::Renderer2D* renderer, HashTable<aie::Texture*>* textureTable)
{
	// Render background
	renderer->drawSprite(m_background, WINDOW_HCENTER, WINDOW_WCENTER, WINDOW_WIDTH, WINDOW_HEIGHT);

	renderer->drawSprite(textureTable->GetData("Simon Base"), WINDOW_HCENTER, WINDOW_WCENTER, 512, 512);
	m_uiManager->Draw(renderer);
	// Refresh colour so buttons don't get perpetually darker
	m_uiManager->ButtonRefresh();
}

// Resets variables to defaults
void PlayingState::OnEnter()
{
	m_uiManager->RemoveTextbox();

	m_simonManager->RefreshSequence();
	m_uiManager->RefreshUIManager();
	// Reset score text
	m_uiManager->GetTextBoxList()->SetText("SCORE: 0");

	m_score = 0;

	m_gameoverVisible = false;
	m_failedSequence = false;

	m_mouseDown = false;
	m_sequencePreview = true;
	m_completePreview = false;
}
