#pragma once
#include "DynamicArray.h"
#include "MEMALLOC_LEAKTEST.h"

template<typename T>
class HashTable
{
public:
	HashTable(unsigned int size);
	~HashTable();

	T& operator[] (char* key)
	{
		auto hashedKey = Hash(key) % m_size;
		return m_data[hashedKey];
	}
	const T& operator[] (char* key) const
	{
		auto hashedKey = hash(key) % m_size;
		return m_data[hashedKey];
	}

	void AddToTable(char* key, T data);
	void RemoveFromTable(char* key);
	T GetData(char* key);
	int GetSize() { return m_size; }

private:
	unsigned int Hash(char* key);

	T*				m_data;
	unsigned int	m_size;
};

template<typename T>
inline HashTable<T>::HashTable(unsigned int size)
{
	m_size = size;
	m_data = DBG_NEW T[size];
	for (int i = 0; i < m_size; i++) { m_data[i] = nullptr; }
}

template<typename T>
inline HashTable<T>::~HashTable()
{
	for (int i = 0; i < m_size; i++) { if(m_data[i]) delete m_data[i]; }
	delete[] m_data;
}

template<typename T>
inline void HashTable<T>::AddToTable(char* key, T data)
{
	m_data[Hash(key)] = data;
}

template<typename T>
inline void HashTable<T>::RemoveFromTable(char * key)
{
	delete m_data[Hash(key)];
}

template<typename T>
inline T HashTable<T>::GetData(char* key)
{
	return m_data[Hash(key)];
}

template<typename T>
inline unsigned int HashTable<T>::Hash(char* key)
{
	unsigned int hash = 0;
	unsigned int sizeOfKey = sizeof(key);

	// Custom hash function
	// Invert the given key
	// Then bitshift left by 2
	hash = ~(*key);
	for (int i = 0; i < sizeOfKey; ++key, i++)
	{
		hash = (hash << 2);
	}
	hash %= m_size;
	return hash;
}
