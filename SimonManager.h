#pragma once
#include "DynamicArray.h"
#include "GameDefinitions.h"
#include <time.h>
#include "Input.h"
#include "Button.h"

class SimonManager
{
public:
	SimonManager(eDifficultyLevel difficulty);
	~SimonManager();
	
	void DifficultyIncrease(eDifficultyLevel difficulty);
	eDifficultyLevel GetDifficulty() { return m_difficulty; }

	bool checkButtonPress(Button* button);

	DynamicArray<int>* GetSequence() { return m_sequenceList; };

	int GetSequencePosID() { return (*m_sequenceList)[m_sequencePos]; }

	// Move sequence position forward by 1
	void ContinueSequence() { m_sequencePos++; }
	// Check if the end of the sequence has been reached
	bool EndOfSequence() { return (m_sequenceList->GetSize() == m_sequencePos); }
	// Reset position for next sequence
	void ResetSequencePos() { m_sequencePos = 0; }

	// Reset sequence back to 0
	void RefreshSequence();

private:
	eDifficultyLevel m_difficulty = eDifficultyLevel::EASY;

	// Array of each flash of the current sequence
	int m_sequencePos = 0;
	DynamicArray<int>* m_sequenceList;
	// Initialises first sequence
	void SequenceInit(eDifficultyLevel difficulty);
	void AddRandFlash(int count);
};