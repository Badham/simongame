#include "TextBox.h"
#include "MEMALLOC_LEAKTEST.h"

TextBox::TextBox(const char* buttonText, float x, float y, float width, float height, float r, float g, float b, float a)
{
	// Button label
	strcpy_s(m_buttonText, 64, buttonText);

	// Font for label
	m_font = DBG_NEW aie::Font("./font/consolas.ttf", 24);

	// Position & size of button
	m_posX = x;
	m_posY = y;
	m_width = width;
	m_height = height;

	// Colour of button
	colour.R = r;
	colour.G = g;
	colour.B = b;
	colour.A = a;
}

TextBox::~TextBox() 
{
	delete m_font; 
}

void TextBox::Draw(aie::Renderer2D* renderer)
{
	renderer->setRenderColour(colour.R, colour.G, colour.B, colour.A);
	renderer->drawBox(m_posX, m_posY, m_width, m_height);

	//Calculating the centred text position is a bit fiddly.
	float textWidth = m_font->getStringWidth(m_buttonText);
	float textHeight = m_font->getStringHeight(m_buttonText);
	float centredPosX = m_posX - (textWidth * 0.5f) + 2;
	float centredPosY = m_posY - (textHeight * 0.5f) + 5;
	//Draw text on the textbox.
	renderer->setRenderColour(0, 0, 0);
	renderer->drawText(m_font, m_buttonText, centredPosX, centredPosY);

	renderer->setRenderColour(1, 1, 1);
}

void TextBox::SetText(const char* text)
{
	strcpy_s(m_buttonText, text);
}
