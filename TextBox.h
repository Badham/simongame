#pragma once
#include "Renderer2D.h"
#include "Font.h"
#include <string.h>
#include "GameDefinitions.h"

class TextBox
{
public:
	TextBox(const char* buttonText, float x, float y, float width, float height, float r, float g, float b, float a);

	~TextBox();

	void Draw(aie::Renderer2D* renderer);

	void SetText(const char* text);

private:
	aie::Font* m_font;
	char m_buttonText[64];

	float m_posX;
	float m_posY;
	float m_width;
	float m_height;

	Colour colour;
};

