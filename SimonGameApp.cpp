#include "SimonGameApp.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include <time.h>

#include "MenuState.h"
#include "PlayingState.h"

SimonGameApp::SimonGameApp() 
{
	m_textureTable = DBG_NEW HashTable<aie::Texture*>(100);
	srand(time(NULL));
}

SimonGameApp::~SimonGameApp() 
{

}

bool SimonGameApp::startup() 
{
	m_2dRenderer = DBG_NEW aie::Renderer2D();

	// TODO: remember to change this when redistributing a build!
	// the following path would be used instead: "./font/consolas.ttf"
	m_font = DBG_NEW aie::Font("./font/consolas.ttf", 32);

	// Texture array
	int DEBUG = sizeof(aie::Texture);

	m_textureTable->AddToTable("Simon Base", DBG_NEW aie::Texture("./textures/simon_back.png"));
	m_textureTable->AddToTable("Menu BG", DBG_NEW aie::Texture("./textures/simon_bg_menu.png"));
	m_textureTable->AddToTable("Easy BG", DBG_NEW aie::Texture("./textures/simon_bg_easy.png"));
	m_textureTable->AddToTable("Hard BG", DBG_NEW aie::Texture("./textures/simon_bg_hard.png"));
	m_textureTable->AddToTable("Nightmare BG", DBG_NEW aie::Texture("./textures/simon_bg_nightmare.png"));

	// Create game state manager
	m_gameStateManager = DBG_NEW GameStateManager(eGameState::STATE_COUNT);
	m_gameStateManager->RegisterState(eGameState::MENU, DBG_NEW MenuState());
	m_gameStateManager->RegisterState(eGameState::INGAME_EASY, DBG_NEW PlayingState(eDifficultyLevel::EASY, m_textureTable));
	m_gameStateManager->RegisterState(eGameState::INGAME_HARD, DBG_NEW PlayingState(eDifficultyLevel::HARD, m_textureTable));
	m_gameStateManager->RegisterState(eGameState::INGAME_NIGHTMARE, DBG_NEW PlayingState(eDifficultyLevel::NIGHTMARE, m_textureTable));

	m_gameStateManager->PushState(eGameState::MENU);

	return true;
}

void SimonGameApp::shutdown() 
{
	delete m_font;
	delete m_2dRenderer;

	delete m_textureTable;
	delete m_gameStateManager;
}

void SimonGameApp::update(float deltaTime) 
{
	// input example
	aie::Input* input = aie::Input::getInstance();

	m_gameStateManager->Update(deltaTime, input);

	if (m_gameStateManager->ActiveStateCount() <= 0)
		quit();
}

void SimonGameApp::draw() 
{
	// wipe the screen to the background colour
	clearScreen();

	// begin drawing sprites
	m_2dRenderer->begin();

	// draw your stuff here!
	m_gameStateManager->Draw(m_2dRenderer, m_textureTable);

	// done drawing sprites
	m_2dRenderer->end();
}