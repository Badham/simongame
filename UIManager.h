#pragma once
#include "Button.h"
#include "TextBox.h"
#include "DynamicArray.h"

class UIManager
{
public:
	UIManager();
	~UIManager();

	void Draw(aie::Renderer2D* renderer);
	void ButtonRefresh();
	// Update sets the given ptr-ptr to the button being hovered over, if valid
	void Update(Button** buttonHover);

	void DisplaySequence(DynamicArray<int>* sequence, eDifficultyLevel difficulty, float deltaTime);

	Button* GetButtonList() { return (*m_buttonArray)[0]; }
	TextBox* GetTextBoxList() { return (*m_textBoxArray)[0]; }

	bool GetSequenceComplete();
	void SetSequenceComplete(bool state);

	void AddButton(int id, const char* buttonText, float x, float y, float width, float height, float r = 0.0f, float g = 0.0f, float b = 0.0f, float a = 1.0f);
	void AddTextBox(const char* buttonText, float x, float y, float width, float height, float r = 0.0f, float g = 0.0f, float b = 0.0f, float a = 1.0f);

	void RemoveButton(int id);
	void RemoveTextbox();

	void RefreshUIManager();

private:
	DynamicArray<Button*>* m_buttonArray;
	DynamicArray<TextBox*>* m_textBoxArray;

	float		m_displayTime = 0.0f;
	int			m_sequencePos = -1;
	bool		m_sequenceComplete = false;

	bool		m_sequenceFailed = false;
};

