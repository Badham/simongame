#pragma once
#include "GameState.h"
#include "SimonManager.h"
#include "UIManager.h"

class PlayingState : public GameState
{
public:
	PlayingState(eDifficultyLevel difficulty, HashTable<aie::Texture*>* textureTable);
	// TODO: FIX EXCEPTION HERE FROM BACKGROUND
	virtual ~PlayingState() 
	{ 
		delete m_simonManager; 
		delete m_uiManager; 
	};

	virtual void OnUpdate(float deltaTime, aie::Input* input);
	virtual void OnDraw(aie::Renderer2D* renderer, HashTable<aie::Texture*>* textureTable);

	virtual void OnEnter();

private:
	SimonManager*		m_simonManager;
	UIManager*			m_uiManager;
	eDifficultyLevel	m_difficulty;

	aie::Texture*		m_background;

	uint32_t			m_score;

	bool				m_failedSequence = false;
	bool				m_gameoverVisible = false;

	bool				m_mouseDown = false;
	bool				m_sequencePreview = true;
	bool				m_completePreview = false;
};